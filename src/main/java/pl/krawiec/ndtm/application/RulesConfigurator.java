package pl.krawiec.ndtm.application;

import pl.krawiec.ndtm.domain.model.instructiontable.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RulesConfigurator
{
	static Map<State, StateRules> configureRules( Map<Character, Symbol> alphabet, Map<String, State> states )
	{
		String line;
		Map<State, StateRules> stateRulesMap = new HashMap<State, StateRules>();
		AppConfigurator.OUT.println( "Podaj zasady:" );
		do
		{
			line = AppConfigurator.in.nextLine();
			if( !isEnd( line ))
			{
				StateRules stateRules = parseLine( line, alphabet, states );
				stateRulesMap.put( stateRules.getState(), stateRules );
			}
		}
		while ( !isEnd( line ) );
		return stateRulesMap;
	}

	private static boolean isEnd( String line )
	{
		return line.startsWith( "end" );
	}


	static private final Pattern statePattern = Pattern.compile( "(\\S+)\\{(\\S*)\\}" );
	static private final Pattern rulePattern = Pattern.compile( "(\\S+)\\[(\\S+)\\]" );
	static private final Pattern transitionPattern = Pattern.compile( "(\\S+)-([L|R])>(\\S+)" );

	private static StateRules parseLine( String line, Map<Character, Symbol> alphabet, Map<String,State> stateMap )
	{
		//przyklad: "A{B[0-L>1,0-L>0];C[1-L>1]}

		Matcher matcher = matcher = statePattern.matcher( line );
		if ( matcher.matches() )
		{
			String stateName = matcher.group( 1 );
			State state = stateMap.get( stateName );
			if(state==null) throwIllegalArgumentException( stateName );

			String rulesForStates = matcher.group( 2 );
			Map<Symbol, List<Instruction>> rules = new HashMap<>();
			for ( String stateRules : rulesForStates.split( ";" ) )
			{
				matcher = rulePattern.matcher( stateRules );
				if ( matcher.matches() )
				{
					String nextStateName = matcher.group( 1 );
					State nextState = stateMap.get( nextStateName );
					String stateTransitions = matcher.group( 2 );
					for ( String stateTransition : stateTransitions.split( "," ) )
					{
						matcher = transitionPattern.matcher( stateTransition );
						if(matcher.matches())
						{
							String matchingSymbolString = matcher.group( 1 );
							String directionString = matcher.group( 2 );
							String replacingSymbolString = matcher.group( 3 );

							Symbol matchingSymbol = alphabet.get( matchingSymbolString.charAt( 0 ) );
							Symbol replacingSymbol = alphabet.get( replacingSymbolString.charAt( 0 ) );
							HeadDirection headDirection = null;
							switch ( directionString )
							{
							case "L":
								headDirection = HeadDirection.LEFT;
								break;
							case "R":
								headDirection = HeadDirection.RIGHT;
								break;
							default:
								throwIllegalArgumentException( directionString );
							}
							List<Instruction> instructionsForSymbol = rules.get( matchingSymbol );
							if(instructionsForSymbol==null){
								instructionsForSymbol = new ArrayList<>(  );
								rules.put( matchingSymbol, instructionsForSymbol );
							}
							instructionsForSymbol.add( new Instruction( matchingSymbol, replacingSymbol, nextState, headDirection ));
						}else{
							throwIllegalArgumentException( stateTransition );
						}
					}
				}
				else
				{
					throwIllegalArgumentException( line );
				}
			}
			return new StateRules( state, rules );

		}

		throwIllegalArgumentException( "" );
		return null;
	}

	private static void throwIllegalArgumentException( String line )
	{
		throw new IllegalArgumentException( String.format( "Wrong format of a rule %s", line ) );
	}

}