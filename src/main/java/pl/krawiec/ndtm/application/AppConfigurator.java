package pl.krawiec.ndtm.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.krawiec.ndtm.domain.model.instructiontable.*;
import pl.krawiec.ndtm.domain.model.machine.*;

import java.io.PrintStream;
import java.util.*;

/**
 * Created by damian on 6/5/15.
 */

@Service
public class AppConfigurator
{
	public static final Scanner in = new Scanner( System.in );
	public static final PrintStream OUT = System.out;

	@Autowired
	MachineFactory machineFactory;
	@Autowired
	MachineRepository machineRepository;
	@Autowired
	InstructionTableFactory instructionTableFactory;
	@Autowired
	InstructionTableRepository instructionTableRepository;

	@Autowired
	TapeFactory tapeFactory;

	public MachineID configure()
	{
		Map<Character, Symbol> alphabet = configureAlphabet();
		Map<String, State> states = configureStates();
		Map<State, StateRules> rules = RulesConfigurator.configureRules( alphabet, states );
		State startingState = configureStartingState( states );
		Tape tape = configureTape( alphabet );
		return configure( tape, startingState, rules );
	}

	private Map<String, State> configureStates()
	{
		Map<String, State> stateMap = new HashMap<String, State>();
		OUT.println( "Podaj stany akceptujace" );
		parseStates( stateMap, true, true );

		OUT.println( "Podaj stany odrzucajace" );
		parseStates( stateMap, true, false );

		OUT.println( "Podaj wszystkie inne stany" );
		parseStates( stateMap, false, false );
		return stateMap;
	}

	private static void parseStates( Map<String, State> stateMap, boolean endState, boolean acceptingStates )
	{
		String line = AppConfigurator.in.nextLine();
		while(line.trim().length()==0) line = in.nextLine();

		for ( String stateName : line.split( "," ) )
		{
			State state = new State( endState, acceptingStates, stateName );
			stateMap.put( stateName, state );
		}
	}
//Y{X[a-R>a];Z[b-R>b]}
	private Map<Character, Symbol> configureAlphabet()
	{
		Map<Character, Symbol> alphabet = new HashMap<>();
		OUT.println( "Podaj alfabet:" );
		String line = in.nextLine();
		for ( char c : line.toCharArray() )
		{
			alphabet.put( c, new Symbol( c ) );
		}
		return alphabet;
	}

	private State configureStartingState(Map<String, State> states )
	{
		OUT.println( "Podaj stan poczatkowy:" );
		String startingStateName = in.next();
		State state = states.get( startingStateName );
		if ( state == null )
		{
			throw new IllegalArgumentException( "Nie znaleziono elementu" );
		}
		return state;
	}

	private Tape configureTape( Map<Character, Symbol> alphabet )
	{
		List<Symbol> symbols = new ArrayList<>();
		OUT.println( "Podaj tasme:" );
		String line;
		do{ line = in.nextLine();}while ( line.trim().length()==0 );
		for ( char c : line.toCharArray() )
		{
			Symbol s = alphabet.get( c );
			if ( s == null )
			{
				throw new IllegalArgumentException( String.format( "Zla litera %c", c ) );
			}
			symbols.add( s );
		}
		return tapeFactory.create( symbols );
	}

	private MachineID configure( Tape tape, State StartingState, Map<State, StateRules> rules )
	{
		InstructionTable instructionTable = instructionTableFactory.create( StartingState, rules );
		instructionTableRepository.save( instructionTable );
		Machine machine = machineFactory.create( instructionTable.getId(), new MachineState( 0, tape, instructionTable.getStartingState() ) );
		machineRepository.save( machine );
		return machine.getId();
	}

}
