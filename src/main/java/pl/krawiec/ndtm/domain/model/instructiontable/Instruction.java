package pl.krawiec.ndtm.domain.model.instructiontable;

import pl.krawiec.ndtm.domain.model.Entity;

/**
 * Created by damian on 6/5/15.
 */
public class Instruction implements Entity
{
	final Symbol match;
	final Symbol replace;
	final State nextState;
	final HeadDirection direction;

	public Instruction( Symbol match, Symbol replace, State nextState, HeadDirection direction )
	{
		this.match = match;
		this.replace = replace;
		this.nextState = nextState;
		this.direction = direction;
	}

	public Symbol getMatch()
	{
		return match;
	}

	public Symbol getReplace()
	{
		return replace;
	}

	public State getNextState()
	{
		return nextState;
	}

	public HeadDirection getDirection()
	{
		return direction;
	}

	@Override
	public boolean equals( Object o )
	{
		if ( this == o )
			return true;
		if ( o == null || getClass() != o.getClass() )
			return false;

		Instruction that = ( Instruction ) o;

		if ( direction != that.direction )
			return false;
		if ( match != null ? !match.equals( that.match ) : that.match != null )
			return false;
		if ( nextState != null ? !nextState.equals( that.nextState ) : that.nextState != null )
			return false;
		if ( replace != null ? !replace.equals( that.replace ) : that.replace != null )
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = match != null ? match.hashCode() : 0;
		result = 31 * result + ( replace != null ? replace.hashCode() : 0 );
		result = 31 * result + ( nextState != null ? nextState.hashCode() : 0 );
		result = 31 * result + ( direction != null ? direction.hashCode() : 0 );
		return result;
	}

	@Override
	public String toString()
	{
		return "Instruction{" +
				"match=" + match +
				", replace=" + replace +
				", nextState=" + nextState +
				", direction=" + direction +
				'}';
	}
}
