package pl.krawiec.ndtm.domain.model.machine;

import pl.krawiec.ndtm.domain.model.Entity;
import pl.krawiec.ndtm.domain.model.instructiontable.Symbol;

import java.util.List;

/**
 * Created by damian on 6/5/15.
 */
public class Tape implements Entity
{
	private final int headPosition;
	private final List<Cell> cells;

	Tape( int headPosition, List<Cell> cells )
	{
		this.headPosition = headPosition;
		this.cells = cells;
	}

	public Symbol readHead()
	{
		return cells.get( headPosition ).getSymbol();
	}

	int getHeadPosition()
	{
		return headPosition;
	}

	List<Cell> getCells()
	{
		return cells;
	}

	@Override
	public boolean equals( Object o )
	{
		if ( this == o )
			return true;
		if ( o == null || getClass() != o.getClass() )
			return false;

		Tape tape = ( Tape ) o;

		if ( headPosition != tape.headPosition )
			return false;
		if ( cells != null ? !cells.equals( tape.cells ) : tape.cells != null )
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = headPosition;
		result = 31 * result + ( cells != null ? cells.hashCode() : 0 );
		return result;
	}

	@Override
	public String toString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		int i = 0;
		for ( Cell cell : cells )
		{
			if ( i == headPosition )
			{
				stringBuilder.append( "{" );
				stringBuilder.append( cell );
				stringBuilder.append( "}" );
			}
			else
			{
				stringBuilder.append( "|" );
				stringBuilder.append( cell );
				stringBuilder.append( "|" );
			}
			i++;
		}

		return stringBuilder.toString();
	}
}
