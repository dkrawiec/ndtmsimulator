package pl.krawiec.ndtm.domain.model.machine;

import pl.krawiec.ndtm.domain.model.Entity;

import java.util.List;

/**
 * Created by damian on 6/5/15.
 */
public class MachineHistory implements Entity
{
	private final List<MachineState> stateList;

	public MachineHistory( List<MachineState> stateList )
	{
		this.stateList = stateList;
	}

	public void addMachineState( MachineState machineState )
	{
		stateList.add( machineState );
	}

	public List<MachineState> getStateList()
	{
		return stateList;
	}

	@Override
	public String toString()
	{
		return "MachineHistory{" +
				"stateList=" + stateList +
				'}';
	}
}
