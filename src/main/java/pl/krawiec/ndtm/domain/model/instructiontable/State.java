package pl.krawiec.ndtm.domain.model.instructiontable;

import pl.krawiec.ndtm.domain.model.ValueObject;

/**
 * Created by damian on 6/5/15.
 */
public class State implements ValueObject
{
	private final boolean endState;
	private final boolean acceptingState;
	private final String stateName;

	public State( boolean endState, boolean acceptingState, String stateName )
	{
		this.endState = endState;
		this.acceptingState = acceptingState;
		this.stateName = stateName;
	}

	public boolean isEndState()
	{
		return endState;
	}

	public boolean isAcceptingState()
	{
		return acceptingState;
	}

	public String getStateName()
	{
		return stateName;
	}

	@Override
	public boolean equals( Object o )
	{
		if ( this == o )
			return true;
		if ( o == null || getClass() != o.getClass() )
			return false;

		State state = ( State ) o;

		if ( stateName != null ? !stateName.equals( state.stateName ) : state.stateName != null )
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		return stateName != null ? stateName.hashCode() : 0;
	}

	@Override
	public String toString()
	{
		return "State{" +
				"endState=" + endState +
				", acceptingState=" + acceptingState +
				", stateName='" + stateName + '\'' +
				'}';
	}
}
