package pl.krawiec.ndtm.domain.model.machine;

import org.springframework.stereotype.Component;
import pl.krawiec.ndtm.domain.model.instructiontable.Symbol;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by damian on 6/5/15.
 */
@Component
public class TapeFactory
{
	public static final Symbol EMPTY_CHAR_SYMBOL = new Symbol( '#' );

	public Tape createAndReplace( Tape tape, int position, Symbol replacingSymbol )
	{
		List<Cell> cells = tape.getCells();
		List<Cell> newCells = new ArrayList<>();
		if ( position < 0 )
		{
			cells.add( new Cell( EMPTY_CHAR_SYMBOL ) );
			position++;
		}
		int i = tape.getHeadPosition();
		for ( Cell c : cells )
		{
			if ( i == 0 )
			{
				newCells.add( new Cell( replacingSymbol ) );
			}
			else
			{
				newCells.add( new Cell( c.getSymbol() ) );
			}
			i--;
		}
		int maxIndex = cells.size() - 1;
		if ( position > maxIndex )
		{
			newCells.add( new Cell( EMPTY_CHAR_SYMBOL ) );
		}
		return new Tape( position, newCells );
	}

	public Tape create( List<Symbol> symbols )
	{
		List<Cell> cells = new ArrayList<>();
		for ( Symbol s : symbols )
		{
			cells.add( new Cell( s ) );
		}
		return new Tape( 0, cells );
	}
}
