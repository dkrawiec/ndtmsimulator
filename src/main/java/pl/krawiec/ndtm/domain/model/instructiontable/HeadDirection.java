package pl.krawiec.ndtm.domain.model.instructiontable;

import pl.krawiec.ndtm.domain.model.ValueObject;

/**
 * Created by damian on 6/5/15.
 */
public enum HeadDirection implements ValueObject
{
	LEFT,RIGHT
}
