package pl.krawiec.ndtm.domain.model.machine;

import pl.krawiec.ndtm.domain.model.ValueObject;

/**
 * Created by damian on 6/5/15.
 */
public class MachineID implements ValueObject
{
	final private String machineId;

	public MachineID( String machineId )
	{
		this.machineId = machineId;
	}

	public String getMachineId()
	{
		return machineId;
	}

	@Override
	public boolean equals( Object o )
	{
		if ( this == o )
			return true;
		if ( o == null || getClass() != o.getClass() )
			return false;

		MachineID machineID = ( MachineID ) o;

		if ( machineId != null ? !machineId.equals( machineID.machineId ) : machineID.machineId != null )
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		return machineId != null ? machineId.hashCode() : 0;
	}

	@Override
	public String toString()
	{
		return String.format("MachineID: '%s'",machineId);
	}
}
