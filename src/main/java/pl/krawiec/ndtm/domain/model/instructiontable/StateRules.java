package pl.krawiec.ndtm.domain.model.instructiontable;

import pl.krawiec.ndtm.domain.model.Entity;

import java.util.List;
import java.util.Map;

/**
 * Created by damian on 6/5/15.
 */
public class StateRules implements Entity
{
	final private State state;
	final private Map<Symbol, List<Instruction>> rules;

	public StateRules( State state, Map<Symbol, List<Instruction>> rules )
	{
		this.state = state;
		this.rules = rules;
	}

	public List<Instruction> getInstructionsForSymbol( Symbol symbol )
	{
		return rules.get( symbol );
	}

	public State getState()
	{
		return state;
	}

	@Override
	public boolean equals( Object o )
	{
		if ( this == o )
			return true;
		if ( o == null || getClass() != o.getClass() )
			return false;

		StateRules that = ( StateRules ) o;

		if ( state != null ? !state.equals( that.state ) : that.state != null )
			return false;
		if ( rules != null ? !rules.equals( that.rules ) : that.rules != null )
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = state != null ? state.hashCode() : 0;
		result = 31 * result + ( rules != null ? rules.hashCode() : 0 );
		return result;
	}

	@Override
	public String toString()
	{
		return "StateRules{" +
				"state=" + state +
				", rules=" + rules +
				'}';
	}
}
