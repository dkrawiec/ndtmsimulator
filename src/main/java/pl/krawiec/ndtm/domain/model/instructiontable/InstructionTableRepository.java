package pl.krawiec.ndtm.domain.model.instructiontable;

import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by damian on 6/5/15.
 */
@Repository
public class InstructionTableRepository
{
	private Map<InstructionTableID, InstructionTable> instructionTableMap = new HashMap<>(  );

	public void save( InstructionTable instructionTable )
	{
		instructionTableMap.put( instructionTable.getId(), instructionTable );
	}

	public InstructionTable load( InstructionTableID instructionTableID )
	{
		return instructionTableMap.get( instructionTableID );
	}
}
