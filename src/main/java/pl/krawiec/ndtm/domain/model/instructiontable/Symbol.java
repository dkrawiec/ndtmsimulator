package pl.krawiec.ndtm.domain.model.instructiontable;

import pl.krawiec.ndtm.domain.model.ValueObject;

/**
 * Created by damian on 6/5/15.
 */
public class Symbol implements ValueObject
{
	private final char aChar;

	public Symbol( char aChar )
	{
		this.aChar = aChar;
	}

	@Override
	public boolean equals( Object o )
	{
		if ( this == o )
			return true;
		if ( o == null || getClass() != o.getClass() )
			return false;

		Symbol symbol1 = ( Symbol ) o;

		if ( aChar != symbol1.aChar )
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		return ( int ) aChar;
	}

	@Override
	public String toString()
	{
		return String.valueOf( aChar );
	}
}
