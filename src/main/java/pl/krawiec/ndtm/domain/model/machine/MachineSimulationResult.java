package pl.krawiec.ndtm.domain.model.machine;

import pl.krawiec.ndtm.domain.model.Entity;

import java.util.List;

/**
 * Created by damian on 6/6/15.
 */
public class MachineSimulationResult implements Entity
{
	private final MachineID machineID;
	private final MachineHistory simulationHistory;

	public MachineSimulationResult( MachineID machineID, MachineHistory simulationHistory )
	{
		this.machineID = machineID;
		this.simulationHistory = simulationHistory;
	}

	public MachineID getMachineID()
	{
		return machineID;
	}

	public MachineState getEndingState()
	{
		List<MachineState> stateList = simulationHistory.getStateList();
		return stateList.get(stateList.size()-1);
	}

	public MachineHistory getSimulationHistory()
	{
		return simulationHistory;
	}

	@Override
	public boolean equals( Object o )
	{
		if ( this == o )
			return true;
		if ( o == null || getClass() != o.getClass() )
			return false;

		MachineSimulationResult result = ( MachineSimulationResult ) o;

		if ( machineID != null ? !machineID.equals( result.machineID ) : result.machineID != null )
			return false;
		if ( simulationHistory != null ? !simulationHistory.equals( result.simulationHistory ) : result.simulationHistory != null )
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = machineID != null ? machineID.hashCode() : 0;
		result = 31 * result + ( simulationHistory != null ? simulationHistory.hashCode() : 0 );
		return result;
	}

	@Override
	public String toString()
	{
		return "MachineSimulationResult{" +
				"machineID=" + machineID +
				", simulationHistory=" + simulationHistory +
				'}';
	}
}
