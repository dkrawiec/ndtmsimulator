package pl.krawiec.ndtm.domain.model.machine;

import pl.krawiec.ndtm.domain.model.Aggregate;
import pl.krawiec.ndtm.domain.model.instructiontable.InstructionTableID;
import pl.krawiec.ndtm.domain.model.instructiontable.State;

/**
 * Created by damian on 6/5/15.
 */
public class Machine implements Aggregate<MachineID>
{
	private final MachineID id;
	private final InstructionTableID instructionTableID;
	private final MachineHistory machineHistory;
	private MachineState actualState;

	public Machine( MachineID id, InstructionTableID instructionTableID, MachineState actualState, MachineHistory machineHistory )
	{
		this.id = id;
		this.instructionTableID = instructionTableID;
		this.actualState = actualState;
		this.machineHistory = machineHistory;
	}

	void updateMachineState( Tape tape, State state )
	{
		int newIteration = actualState.getIteration() + 1;
		actualState = new MachineState( newIteration, tape, state );
		machineHistory.addMachineState( actualState );
	}

	public boolean hasNextIteration()
	{
		return !actualState.getState().isEndState();
	}

	public boolean isAccepting()
	{
		return actualState.getState().isAcceptingState();
	}

	public InstructionTableID getInstructionTableID()
	{
		return instructionTableID;
	}

	public MachineState getActualState()
	{
		return actualState;
	}

	public MachineHistory getMachineHistory()
	{
		return machineHistory;
	}

	@Override
	public MachineID getId()
	{
		return id;
	}
}
