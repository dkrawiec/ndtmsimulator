package pl.krawiec.ndtm.domain.model.instructiontable;

import pl.krawiec.ndtm.domain.model.Aggregate;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by damian on 6/5/15.
 */
public class InstructionTable implements Aggregate<InstructionTableID>
{
	private final InstructionTableID id;
	private final State startingState;
	private final Map<State, StateRules> rules;

	public InstructionTable( InstructionTableID id, State startingState, Map<State, StateRules> rules )
	{
		this.id = id;
		this.startingState = startingState;
		this.rules = rules;
	}

	public State getStartingState()
	{
		return startingState;
	}


	public List<Instruction> getInstructionForStateAndSymbol( State state, Symbol symbol )
	{
		StateRules stateRules = rules.get( state );
		if(stateRules == null) throw new IllegalArgumentException( "No rules for given state and symbol" );
		List<Instruction> instructionsForSymbol = stateRules.getInstructionsForSymbol( symbol );
		if( instructionsForSymbol == null) instructionsForSymbol = Collections.emptyList();
		return instructionsForSymbol;
	}

	@Override
	public InstructionTableID getId()
	{
		return id;
	}

	@Override
	public boolean equals( Object o )
	{
		if ( this == o )
			return true;
		if ( o == null || getClass() != o.getClass() )
			return false;

		InstructionTable that = ( InstructionTable ) o;

		if ( id != null ? !id.equals( that.id ) : that.id != null )
			return false;
		if ( rules != null ? !rules.equals( that.rules ) : that.rules != null )
			return false;
		if ( startingState != null ? !startingState.equals( that.startingState ) : that.startingState != null )
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + ( startingState != null ? startingState.hashCode() : 0 );
		result = 31 * result + ( rules != null ? rules.hashCode() : 0 );
		return result;
	}

	@Override
	public String toString()
	{
		return "InstructionTable{" +
				"id=" + id +
				", startingState=" + startingState +
				", rules=" + rules +
				'}';
	}
}
