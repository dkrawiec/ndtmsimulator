package pl.krawiec.ndtm.domain.model.instructiontable;

import pl.krawiec.ndtm.domain.model.ValueObject;

/**
 * Created by damian on 6/5/15.
 */
public class InstructionTableID implements ValueObject
{
	private final String id;

	public InstructionTableID( String id )
	{
		this.id = id;
	}

	@Override
	public boolean equals( Object o )
	{
		if ( this == o )
			return true;
		if ( o == null || getClass() != o.getClass() )
			return false;

		InstructionTableID that = ( InstructionTableID ) o;

		if ( id != null ? !id.equals( that.id ) : that.id != null )
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		return id != null ? id.hashCode() : 0;
	}

	@Override
	public String toString()
	{
		return "InstructionTableID{" +
				"id='" + id + '\'' +
				'}';
	}
}
