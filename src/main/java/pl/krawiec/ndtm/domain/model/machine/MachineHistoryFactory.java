package pl.krawiec.ndtm.domain.model.machine;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by damian on 6/5/15.
 */
@Component
public class MachineHistoryFactory
{
	public MachineHistory create( List<MachineState> machineHistory )
	{
		List<MachineState> arrayList = new ArrayList<>(  );
		arrayList.addAll( machineHistory );
		return new MachineHistory( arrayList );
	}
}
