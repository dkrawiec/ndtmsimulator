package pl.krawiec.ndtm.domain.model.machine;

import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by damian on 6/5/15.
 */
@Repository
public class MachineRepository
{
	private Map<MachineID, Machine> machineMap = new HashMap<>();

	public void save( Machine machine )
	{
		machineMap.put( machine.getId(), machine );
	}

	public Machine load( MachineID id )
	{
		return machineMap.get( id );
	}

	public void delete( Machine actualMachine )
	{
		machineMap.remove( actualMachine.getId() );
	}
}
