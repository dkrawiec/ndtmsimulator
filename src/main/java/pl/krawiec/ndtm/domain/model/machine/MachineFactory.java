package pl.krawiec.ndtm.domain.model.machine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.krawiec.ndtm.domain.model.instructiontable.InstructionTableID;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by damian on 6/5/15.
 */
@Component
public class MachineFactory
{
	@Autowired
	MachineHistoryFactory machineHistoryFactory;

	public Machine create(Machine machine){
		MachineHistory machineHistory = machineHistoryFactory.create( machine.getMachineHistory().getStateList() );
		return new Machine( generateMachineId(), machine.getInstructionTableID(), machine.getActualState(), machineHistory );
	}

	public Machine create(InstructionTableID instructionTableID, MachineState actualState ){
		ArrayList<MachineState> machineStateList = new ArrayList<>();
		machineStateList.add( actualState );
		MachineHistory machineHistory = machineHistoryFactory.create( machineStateList );
		return new Machine(generateMachineId(), instructionTableID, actualState, machineHistory );
	}

	private MachineID generateMachineId()
	{
		return new MachineID( UUID.randomUUID().toString() );
	}
}
