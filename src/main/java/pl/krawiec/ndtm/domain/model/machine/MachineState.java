package pl.krawiec.ndtm.domain.model.machine;

import pl.krawiec.ndtm.domain.model.Entity;
import pl.krawiec.ndtm.domain.model.instructiontable.State;

/**
 * Created by damian on 6/5/15.
 */
public class MachineState implements Entity
{
	private final int iteration;
	private final Tape tape;
	private final State state;

	public MachineState( int iteration, Tape tape, State state )
	{
		this.iteration = iteration;
		this.tape = tape;
		this.state = state;
	}

	public int getIteration()
	{
		return iteration;
	}

	public Tape getTape()
	{
		return tape;
	}

	public State getState()
	{
		return state;
	}

	@Override
	public boolean equals( Object o )
	{
		if ( this == o )
			return true;
		if ( o == null || getClass() != o.getClass() )
			return false;

		MachineState that = ( MachineState ) o;

		if ( iteration != that.iteration )
			return false;
		if ( state != null ? !state.equals( that.state ) : that.state != null )
			return false;
		if ( tape != null ? !tape.equals( that.tape ) : that.tape != null )
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = iteration;
		result = 31 * result + ( tape != null ? tape.hashCode() : 0 );
		result = 31 * result + ( state != null ? state.hashCode() : 0 );
		return result;
	}

	@Override
	public String toString()
	{
		return "MachineState{" +
				"iteration=" + iteration +
				", tape=" + tape +
				", state=" + state +
				'}';
	}
}
