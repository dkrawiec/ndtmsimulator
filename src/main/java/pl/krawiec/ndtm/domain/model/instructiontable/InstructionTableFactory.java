package pl.krawiec.ndtm.domain.model.instructiontable;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.UUID;

/**
 * Created by damian on 6/6/15.
 */
@Component
public class InstructionTableFactory
{
	public InstructionTable create( State startingState, Map<State, StateRules> rules ){
		InstructionTableID instructionTableId = generateInstructionTableId();
		return new InstructionTable( instructionTableId, startingState, rules );
	}

	private InstructionTableID generateInstructionTableId()
	{
		return new InstructionTableID( UUID.randomUUID().toString() );
	}
}
