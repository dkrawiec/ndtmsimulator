package pl.krawiec.ndtm.domain.model.machine;

import pl.krawiec.ndtm.domain.model.Entity;
import pl.krawiec.ndtm.domain.model.instructiontable.Symbol;

/**
 * Created by damian on 6/5/15.
 */
public class Cell implements Entity
{
	private final Symbol symbol;

	public Cell( Symbol symbol )
	{
		this.symbol = symbol;
	}

	public Symbol getSymbol()
	{
		return symbol;
	}

	@Override
	public boolean equals( Object o )
	{
		if ( this == o )
			return true;
		if ( o == null || getClass() != o.getClass() )
			return false;

		Cell cell = ( Cell ) o;

		if ( symbol != null ? !symbol.equals( cell.symbol ) : cell.symbol != null )
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		return symbol != null ? symbol.hashCode() : 0;
	}

	@Override
	public String toString()
	{
		return String.valueOf( symbol );
	}
}
