package pl.krawiec.ndtm.domain.model;

/**
 * Created by damian on 6/5/15.
 */
public interface Aggregate<T>
{
	T getId();
}
