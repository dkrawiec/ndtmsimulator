package pl.krawiec.ndtm.domain.model.machine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.krawiec.ndtm.domain.model.instructiontable.*;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by damian on 6/5/15.
 */
@Service
public class MachineSimulatorService
{
	@Autowired
	InstructionTableRepository instructionTableRepository;

	@Autowired
	MachineRepository machineRepository;

	@Autowired
	TapeFactory tapeFactory;

	@Autowired
	MachineFactory machineFactory;

	public MachineSimulationResult simulate( MachineID id )
	{
		Deque<MachineID> machineIDQueue = new LinkedList<>();
		machineIDQueue.add( id );
		while ( !machineIDQueue.isEmpty() )
		{
			Machine actualMachine = pollAndRetrieveMachine( machineIDQueue );
			InstructionTable instructionTable = retrieveInstructionTable( actualMachine );
			MachineState actualState = actualMachine.getActualState();
			Symbol currentSymbol = actualState.getTape().readHead();
			State machineInState = actualState.getState();
			List<Instruction> instructionList = instructionTable.getInstructionForStateAndSymbol( machineInState, currentSymbol );
			int instructionListSize = instructionList.size();
			if ( instructionListSize == 1 )
			{
				Instruction instruction = instructionList.get( 0 );
				if ( updateMachine( actualMachine, instruction ) )
				{
					return new MachineSimulationResult( actualMachine.getId(), actualMachine.getMachineHistory() );
				}
				else
				{
					machineIDQueue.add( actualMachine.getId() );
				}
			}
			else if ( instructionListSize > 1 )
			{
				Machine machine = updateNDTM( machineIDQueue, actualMachine, instructionList );
				if ( machine != null )
				{
					return new MachineSimulationResult( machine.getId(), machine.getMachineHistory() );
				}
			}
			else
			{
				System.out.println( "Maszyna stanela na: " + actualMachine.getActualState() );
				machineRepository.delete( actualMachine );
			}
		}

		throw new IllegalStateException( "There is a gremlin in this code" );
	}

	private Machine updateNDTM( Queue<MachineID> machineIDQueue, Machine actualMachine, List<Instruction> instructionList )
	{
		Instruction instruction;
		int instructionListSize = instructionList.size();

		for ( int i = 0; i < instructionListSize; i++ )
		{
			instruction = instructionList.get( i );
			Machine machine = actualMachine;
			if ( i < instructionListSize - 1 )
			{
				machine = machineFactory.create( actualMachine );
				machineRepository.save( machine );
			}
			if ( updateMachine( machine, instruction ) )
			{
				return machine;
			}
			machineIDQueue.add( machine.getId() );
		}
		return null;
	}

	private boolean updateMachine( Machine actualMachine, Instruction instruction )
	{
		MachineState actualState = actualMachine.getActualState();
		Tape tape = actualState.getTape();
		int nextHeadPosition = tape.getHeadPosition();
		switch ( instruction.getDirection() )
		{
		case LEFT:
			nextHeadPosition--;
			break;
		case RIGHT:
			nextHeadPosition++;
			break;
		}
		Tape newTape = tapeFactory.createAndReplace( tape, nextHeadPosition, instruction.getReplace() );
		actualMachine.updateMachineState( newTape, instruction.getNextState() );
		return !actualMachine.hasNextIteration();
	}

	private InstructionTable retrieveInstructionTable( Machine actualMachine )
	{
		return instructionTableRepository.load( actualMachine.getInstructionTableID() );
	}

	private Machine pollAndRetrieveMachine( Deque<MachineID> machineIDQueue )
	{
		MachineID actualMachineId = machineIDQueue.poll();
		return machineRepository.load( actualMachineId );
	}

}
