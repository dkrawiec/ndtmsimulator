package pl.krawiec.ndtm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import pl.krawiec.ndtm.application.AppConfigurator;
import pl.krawiec.ndtm.domain.model.machine.MachineID;
import pl.krawiec.ndtm.domain.model.machine.MachineSimulationResult;
import pl.krawiec.ndtm.domain.model.machine.MachineSimulatorService;
import pl.krawiec.ndtm.domain.model.machine.MachineState;

@SpringBootApplication
@Configuration
@EnableSpringConfigured
@ComponentScan
public class Main
{

	public static void main( String[] args )
	{
		ApplicationContext ctx = startSpringContext( args );
		MachineSimulatorService machineSimulatorService = ctx.getBean( MachineSimulatorService.class );
		AppConfigurator configurator = ctx.getBean( AppConfigurator.class );
		MachineID machineId = configurator.configure();
		MachineSimulationResult result = machineSimulatorService.simulate( machineId );
		printResult( result );
	}

	public static void printResult( MachineSimulationResult result )
	{
		MachineState endingState = result.getEndingState();
		if ( endingState.getState().isAcceptingState() )
		{
			System.out.println( "Zaakceptowano" );
		}
		else
		{
			System.out.println( "Odrzucono" );
		}
		System.out.println( "Historia:" );
		for ( MachineState state : result.getSimulationHistory().getStateList() )
		{
			System.out.println( state );
		}
	}

	private static ApplicationContext startSpringContext( String[] args )
	{
		SpringApplication app = new SpringApplication( Main.class );
		app.setShowBanner( false );
		app.setLogStartupInfo( false );
		app.setRegisterShutdownHook( true );
		return app.run( args );
	}
}
